#include "Dictionary.h"

DictionaryList::node_t* DictionaryList::_insert(node_t* node)
{
    node_t* it = head;
    if (head == nullptr)
    {
        head = node;
        tail = node;
        return node;
    }
    if (node->value == head->value)
    {
        return nullptr; // maybe except
    }
    if (head == tail && node->value > head->value)
    {
        tail = node;
        head->next = node;
        return node;
    }
    if (node->value < head->value)
    {
        node_t* tmp = head;
        head = node;
        node->next = tmp;
        tail = node->next;
        return node;
    }
    while (it->next != nullptr)
    {
        if (node->value == it->next->value) return nullptr;
        if (node->value < it->next->value)
        {
            node_t* tmp = it->next; //
            it->next = node;        // insert new element
            node->next = tmp;       //
            return node;
        }
        it = it->next;
    }
    it->next = node;
    return node;
}

void DictionaryList::_del(node_t* node)
{
    delete node;
}

DictionaryList::node_t* DictionaryList::_search(int val)
{
    node_t* it = head;
    if (head == nullptr) return nullptr;
    while (it->next != nullptr)
    {
        if (it->value == val) return it;
        it = it->next;
    }
    if (it->value == val) return it;
    return nullptr;
}