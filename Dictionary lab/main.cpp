#include <iostream>
#include <string>
#include <regex>
#include <exception>

#include "Dictionary.h"

void testInput(DictionaryList* dict);
void testSearch(DictionaryList* dict);
void testDelete(DictionaryList* dict);
void testMerge(DictionaryList* dict1, DictionaryList* dict2);
void testDeleteMatching(DictionaryList* dict1, DictionaryList* dict2);
DictionaryList testGetIntersect(DictionaryList* dict1, DictionaryList* dict2);

void show2dict(DictionaryList* dict1, DictionaryList* dict2);

std::regex r(R"(-?([1-9][0-9]*|[0-9]))");
std::string input;

int main()
{
    DictionaryList dict1, dict2, dict3;
    testInput(&dict1);
    testInput(&dict2);

    dict3 = testGetIntersect(&dict1, &dict2);
    show2dict(&dict1, &dict2);
    std::cout << "New dict: ";
    dict3.show();

    testDeleteMatching(&dict1, &dict2);

    

    testDelete(&dict3);
    testSearch(&dict3);

    testInput(&dict3);
    testDeleteMatching(&dict3, &dict2);

    return 0;
}

DictionaryList testGetIntersect(DictionaryList* dict1, DictionaryList* dict2)
{
    std::cout << "\nGet intersection.\n";
    show2dict(dict1, dict2);
    std::cout << "\tGetting...\n";
    return getIntersection(*dict1, *dict2);
}

void show2dict(DictionaryList* dict1, DictionaryList* dict2)
{
    std::cout << "Dict1: ";
    dict1->show();
    std::cout << "Dict2: ";
    dict2->show();
}

void testDeleteMatching(DictionaryList* dict1, DictionaryList* dict2)
{
    std::cout << "\nDelete matching values.\n";
    show2dict(dict1, dict2);
    std::cout << "\tDeleting...\n";
    dict1->deleteMatching(*dict2);
    show2dict(dict1, dict2);
}

void testMerge(DictionaryList* dict1, DictionaryList* dict2)
{
    std::cout << "\nMerge two dicts\n";
    show2dict(dict1, dict2);
    dict1->merge(*dict2);
    std::cout << "\tMerging...\n";
    show2dict(dict1, dict2);
}

void testInput(DictionaryList* dict)
{
    std::cout << "\nEnter elements\n";
    int in = 0;
    bool retCode = false;
    while (true)
    {
        std::cout << ">>> "; 
        std::cin >> input;
        if (input == "end")
        {
            break;
        }
        try
        {
            if (std::regex_match(input, r) == false)
            {
                throw std::runtime_error("Error: Integer required.\n");
            }
        }
        catch (std::runtime_error e)
        {
            std::cerr << e.what();
            continue;
        }
        in = atoi(input.data());
        try 
        {
            retCode = dict->insert(in);
            if (retCode == false)
            {
                throw std::runtime_error("Error: this number is already in dictionary.\n");
            }
        }
        catch (std::runtime_error e)
        {
            std::cerr << e.what();
            continue;
        }
    }
    dict->show();
}

void testSearch(DictionaryList* dict)
{
    std::cout << "\nWhich to search?\n";
    int in = 0;
    while (true)
    {
        std::cout << ">>> "; 
        std::cin >> input;
        if (input == "end")
        {
            break;
        }
        try
        {
            if (std::regex_match(input, r) == false)
            {
                throw std::runtime_error("Error: Integer required.\n");
            }
        }
        catch (std::runtime_error ex)
        {
            std::cerr << ex.what();
            continue;
        }
        in = atoi(input.data());
        if (dict->search(in))
        {
            std::cout << "In\n";
        }
        else
        {
            std::cout << "Out\n";
        }
    }
}

void testDelete(DictionaryList* dict)
{
    int in = 0;
    std::cout << "\nWhich to del?\n";
    while (true)
    {
        std::cout << ">>> ";
        input = " ";
        std::cin >> input;
        if (input == "end")
        {
            break;
        }
        try 
        {
            if (std::regex_match(input, r) == false)
            {
                throw std::runtime_error("Error: Integer required.\n");
            }
        }
        catch (std::runtime_error ex)
        {
            std::cerr << ex.what();
            continue;
        }
        in = atoi(input.data());
        try
        {
            bool ret = dict->del(in);
            if (ret == false)
            {
                throw std::runtime_error("Error: element to delete is not in dict.\n");
            }
        }
        catch (std::runtime_error e)
        {
            std::cerr << e.what();
        }
    }
    dict->show();
}