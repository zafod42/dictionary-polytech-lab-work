#ifndef __DICTIONARY_LIST
#define __DICTIONARY_LIST

class DictionaryList
{
private:
    struct node_t
    {
        node_t* next;
        int value;
        node_t(int val) : value(val), next(nullptr) {}
    };

    node_t* head;
    node_t* tail;

    node_t* _insert(node_t* node);
    void _del(node_t* node);
    node_t* _search(int val);

public:
    DictionaryList();
    DictionaryList(const DictionaryList& rightValue);
    DictionaryList(DictionaryList&& leftValue);
    ~DictionaryList();

    bool insert(int value);
    bool del(int value);
    bool search(int value);

    void show();

    void merge(DictionaryList& right);
    void deleteMatching(DictionaryList& right);

    DictionaryList& operator=(const DictionaryList& right);
    DictionaryList& operator=(DictionaryList&& right);

    friend DictionaryList getIntersection(DictionaryList& left, DictionaryList& right)
    {
        DictionaryList cpLeft = left, cpRight = right;
        DictionaryList ans;
        node_t* it = cpRight.head;
        if (cpLeft.head == nullptr || cpRight.head == nullptr)
        {
            return cpLeft;
        }
        if (cpLeft.head->next == nullptr)
        {
            if (cpRight._search(cpLeft.head->value) != nullptr) 
            {
                return cpLeft;
            }
        }
        if (cpRight.head->next == nullptr)
        {
            if (cpLeft._search(cpRight.head->value) != nullptr)
            {
                return cpRight;
            }
        }
        if (cpLeft._search(cpRight.head->value) != nullptr)
        {
            ans.insert(cpRight.head->value);
        }
        while (it->next != nullptr)
        {
            if (cpLeft._search(it->value) != nullptr)
            {
                ans.insert(it->value);
            }
            it = it->next;
        }
        if (cpLeft._search(it->value) != nullptr)
        {
            ans.insert(it->value);
        }
        return ans;
    }
};

#endif // !__DICTIONARY_LIST