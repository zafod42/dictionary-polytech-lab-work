#include "Dictionary.h"

#include <iostream>

DictionaryList::DictionaryList() : head(nullptr), tail(nullptr) {}


DictionaryList::DictionaryList(const DictionaryList& right)
{
    if (right.head == nullptr) return;
    insert(right.head->value);
    if (right.head->next == nullptr) return;
    node_t* r = right.head->next;
    while (r->next != nullptr)
    {
        insert(r->value);
        r = r->next;
    }
    insert(r->value);
}

DictionaryList& DictionaryList::operator= (const DictionaryList& right)
{
    if (right.head == nullptr) return *this;
    if (right.head->next == nullptr)
    {
        insert(right.head->value);
        return *this;
    }
    node_t* it = right.head;
    insert(it->value);
    while (it->next != nullptr)
    {
        it = it->next;
        insert(it->value);
    }
    return *this;
}

DictionaryList& DictionaryList::operator= (DictionaryList&& right)
{
    if (right.head == nullptr) return *this;
    head = right.head;
    tail = right.tail;
    right.head = nullptr;
    right.tail = nullptr;
    return *this;
}

DictionaryList::DictionaryList(DictionaryList&& lvalue) : head(nullptr), tail(nullptr)
{
    head = lvalue.head;
    tail = lvalue.tail;
    lvalue.head = nullptr;
    lvalue.tail = nullptr;
}


DictionaryList::~DictionaryList()
{
    if (head == nullptr) return;
    while (head->next != nullptr)
    {
        node_t* tmp = head;
        while (tmp->next->next != nullptr)
        {
            tmp = tmp->next;
        }
        delete tmp->next;
        tmp->next = nullptr;
    }
    delete head;
    head = nullptr;
}

bool DictionaryList::insert(int value)
{
    node_t* node = new node_t(value);
    if (_insert(node) != nullptr)
    {
        return true;
    }
    delete node;
    return false;
}

bool DictionaryList::del(int value)
{
    if (head == nullptr)
    {
        return true;
    }
    node_t* tmp = head;
    node_t* prev = tmp;
    if (tmp->value == value)
    {
        node_t* next = tmp->next;
        head = next;
        _del(tmp);
        return true;
    }
    while (tmp->value != value && tmp->next != nullptr)   
    {
        prev = tmp;
        tmp = tmp->next;
    }

    /* return if value not found */
    if (tmp->value != value) return false;
    if (tmp->next == nullptr)
    {
        prev->next = nullptr;
        _del(tmp);
        return true;
    }
    prev->next = tmp->next;
    _del(tmp);
    return true; // edit
}

void DictionaryList::show()
{
    node_t* tmp = head;
    if (tmp == nullptr)
    {
        std::cout << "Dict is empty\n";
        return;
    }
    if (tmp->next == nullptr)
    {
        std::cout << tmp->value << '\n';
        return;
    }
    while (tmp->next != nullptr)
    {
        std::cout << tmp->value << ' ';
        tmp = tmp->next;
    }
    std::cout << tmp->value <<'\n';
}

bool DictionaryList::search(int value)
{
    if (_search(value) != nullptr)
    {
        return true;
    }
    return false;
}

void DictionaryList::merge(DictionaryList& right)
{
    node_t* it = right.head;
    while (it != nullptr)
    {
        it = right.head;
        if (right.head != nullptr) 
        {
            right.head = right.head->next;
            _insert(it);
        }
    }
}

void DictionaryList::deleteMatching(DictionaryList& right)
{
    if (right.head == nullptr) return;
    if (this->head == nullptr) return;

    node_t* it = right.head;
    while (it->next != nullptr)
    {
        del(it->value);
        it = it->next;
    }
    del(it->value);
}

//DictionaryList getIntersection(const DictionaryList& left, const DictionaryList& right)
//{
//    DictionaryList cpLeft = left, cpRight = right;
//    cpLeft.merge(cpRight);
//    return cpLeft;
//}

//TODO: delete matching, get insertion
//      doc file of lab work.
//      everything at 16.02.2023
//      deadline 17.02.2023 15.00